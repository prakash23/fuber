package fuber

class DistanceUtils {
   public static double getDistanceBetweencoordinates(List coordinate1, List coordinate2){
        Math.sqrt(Math.pow(getDifferenceInXcoordinate(coordinate1,coordinate2),2) + Math.pow(getDifferenceInYcoordinate(coordinate1,coordinate2),2))
    }

    public static double getDifferenceInXcoordinate(List coordinate1, List coordinate2){
        coordinate1.first() - coordinate2.first()
    }
    public static double getDifferenceInYcoordinate(List coordinate1, List coordinate2){
        coordinate1.last() - coordinate2.last()
    }
}
