<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="container">
<div class="row">
    <div class="col-xs-6">
        <table class="table table-hover table-condensed">
            <caption>All Cabs In the System</caption>
            <thead>
            <tr>
                <th>Car Id</th>
                <th>Location</th>
                <th>is Pink?</th>
                <th>Available?</th>
            </tr>
            </thead>
        <tbody>
            <g:each in="${cabLocations}" var="cabLocation">
                <tr>
                    <td>${cabLocation.cab.id}</td>
                    <td>${cabLocation.coordinate.first()} , ${cabLocation.coordinate.last()}</td>
                    <td>${cabLocation.cab.isPink ? "Yes" : "No"}</td>
                    <td>${cabLocation.isActive ? "Yes" : "No"}</td>
                </tr>
                </tbody>
            </g:each>
        </table>
    </div>
    <div class="col-xs-6">
        row2
    </div>
</div>
    </div>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>