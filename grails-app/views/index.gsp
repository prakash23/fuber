<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="container">
    <h1>Welcome to Fuber Cabs</h1>
    <div class="row">
        <h2>Cabs API</h2>
        <ul>
            <li>
            Get list of all Cabs -  /cabs  (GET)
            </li>
            <li>
                Get Details of a cab -  /cabs/[cabId] (GET)
            </li>
            </ul>
    </div>

    <div class="row">
        <h2>Reservations API</h2>
        <ul>
            <li>
                Get list of all reservations -  /reservations (GET)
            </li>
            <li>
                Get Details of a reservation -  /reservations/[resId] (GET)
            </li>
        </ul>
    </div>

  <div class="row">
        <h2>Start and End Reservations</h2>
        <ul>
            <li>
                Make a reservation -  /reservation/start (POST)
                <pre>
               <p> Sample Request JSON - {location:[2.5,3.5] , is Pink :true}</p>
               <p> Sample Request JSON - {"msg":"Cab found","cabId":19,"distance":0.0,"resId":2}</p>
                </pre>
            </li>
            <li>
                End a reservation -  /reservation/end (POST)
                <pre>
                    <p> Sample Request JSON - {location:[2.5,3.5] , resId :"1"}</p>
                    <p> Sample Request JSON - {"resId":2,"isPink":true,"start":1438459615103,"end":1438459953981,"startLocation":[7.0,8.0],"endLocation":[7.0,8.0],"amount":10.0,"isRunning":false}</p>
                </pre>
            </li>
        </ul>
    </div>


</div>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>