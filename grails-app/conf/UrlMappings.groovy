class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "/reservations"(controller:'reservation',action: 'list')
        "/reservations/$id"(controller:'reservation',action: 'show')
        "/cabs"(controller:'cab',action: 'list')
        "/cabs/$id"(controller:'cab',action: 'show')

    }

}
