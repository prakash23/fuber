import fuber.Cab
import fuber.CabLocation

class BootStrap {

    def init = { servletContext ->

        Random random = new Random()
       List<Cab> cabs= (1..25).collect{
            new Cab(isPink: (random.nextInt()%2==0)).save(flush: true,failOnError: true)
        }

        cabs.each{
            new CabLocation(cab: it,isActive: true,coordinate: [random.nextInt()%500,random.nextInt()%500]).save(flush: true,failOnError: true)
        }
    }
    def destroy = {
    }
}
