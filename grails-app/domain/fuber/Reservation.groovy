package fuber

class Reservation {

    Cab cab
    Date start
    Date end
    Boolean isRunning=true
    List startCoordinate
    List endCoordinate
    Boolean isPink


    static hasMany = [startCoordinate:Double,endCoordinate:Double]

    static constraints = {
        end(nullable: true)
        endCoordinate(nullable: true)
    }
}
