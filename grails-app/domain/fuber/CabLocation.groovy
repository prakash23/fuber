package fuber

class CabLocation {

    Cab cab
    List<Double> coordinate
    Boolean isActive

    static hasMany = [coordinate:Double]

    static constraints = {
        coordinate size:(2..2)
    }
}
