package fuber

import grails.converters.JSON

class CabController {

    static allowedMethods = [list: 'GET',show: 'GET']

    def list(){
        render (Cab.list().collect{
            getCabJSON(it)
        } as JSON)
    }

    def show(Long id){
        Cab cab=Cab.get(id)
        render (getCabJSON(cab) as JSON)
    }

    private Map getCabJSON(Cab cab){
        [cabId:cab?.id,isPink:cab?.isPink,currentLocation:CabLocation.findByCabAndIsActive(cab,true)?.coordinate,isCurrentlyBooked:Reservation.countByCabAndIsRunning(cab,true) as Boolean]
    }
}
