package fuber

import grails.converters.JSON

class ReservationController {

    def reservationService
    static allowedMethods = [list: 'GET',cab: 'POST',end:'POST']

    def list(){
        render (Reservation.list().collect{
            getReservationJSON(it)
        } as JSON)
    }

    def show(Long id){
        Map response=[:]
        if(id){
        Reservation reservation=Reservation.get(id)
           response= getReservationJSON(reservation)
        }
        render (response as JSON)
    }

    def index() {

        [cabLocations:CabLocation.list()]
    }

    def start(){
        Map paramsMap=request.JSON
        List location=paramsMap['location']
        Boolean isPink=paramsMap['isPink']
        def searchResponse=reservationService.getAvailableCar(location,isPink)
        render (searchResponse as JSON)
    }

    def end(){
        Map paramsMap=request.JSON
        List<Double> location=paramsMap['location']
        Reservation reservation=Reservation.get(paramsMap['resId'].toString())
        reservation =reservationService.endReservation(reservation,location)
        render (getReservationJSON(reservation) as JSON)
    }

    private Map getReservationJSON(Reservation it){
        [resId:it?.id,isPink:it?.isPink,start: it?.start?.time,end: it?.end?.time,startLocation:it?.startCoordinate,endLocation:it?.endCoordinate,amount:reservationService.getAmountForReservation(it),isRunning:it?.isRunning]
    }
}
