package fuber

import grails.transaction.Transactional

@Transactional
class ReservationService {


    final int PERMINCHARGE = 1, PERKMCHARGE = 2, PINKCHARGE = 5 // can be in different file

    def getAvailableCar(List searchcoordinates, Boolean shouldBePink) {
        List<Cab> filterCabs = getCabsAvailableForReservation(shouldBePink)
        def cabWithDistance
        String message = "No Cab found with given search criteria"
        def resId=""
        if (filterCabs) {
            cabWithDistance = filterCabs.collectEntries {
                [(it): DistanceUtils.getDistanceBetweencoordinates(searchcoordinates, CabLocation.findByCabAndIsActive(it, true).coordinate)]
            }.min { it.value }
        }
        if (cabWithDistance) {
            message = "Cab found"
            resId = createReservation(cabWithDistance.key, searchcoordinates, shouldBePink)?.id
        }
        [msg: message, cabId: cabWithDistance?.key?.id, distance: cabWithDistance?.value, resId: resId]
    }

    List<Cab> getCabsAvailableForReservation(Boolean shouldBePink) {
        List<Cab> bookedCabs = Reservation.findAllByIsRunning(true)*.cab
        List<Cab> availableCabs = Cab.list() - bookedCabs
        shouldBePink ? availableCabs.findAll { it.isPink } : availableCabs
    }

    Reservation createReservation(Cab cab, List startCoordinate, Boolean isPink) {
        new Reservation(start: new Date(), cab: cab, startCoordinate: startCoordinate, isPink: isPink).save()
    }


    def endReservation(Reservation reservation, List endCoordinate) {
        if(reservation && reservation.isRunning){
        reservation.isRunning = false
        reservation.endCoordinate = endCoordinate.collect{it as Double}
        reservation.end = new Date()
        reservation.save(failOnError: true,flush: true)
        setLocationForCab(reservation.cab, endCoordinate)
        }
        reservation
    }

    double getDistanceTravelledInReservation(Reservation reservation) {
        DistanceUtils.getDistanceBetweencoordinates(reservation.endCoordinate, reservation.startCoordinate)
    }

    def setLocationForCab(Cab cab, List location) {
        inActiveCurrentLocationForCab(cab)
        new CabLocation(cab: cab, coordinate: location, isActive: true).save(failOnError: true,flush: true)
    }

    def inActiveCurrentLocationForCab(Cab cab) {
        CabLocation.findAllByCab(cab).each {
            it.isActive = false
            it.save(failOnError: true, flush: true)
        }
    }

    double getAmountForReservation(Reservation reservation) {
    reservation?(reservation?.isRunning?0:getKMChrageForReservation(reservation)  + getMinuteChrageForReservation(reservation)+getPinkChargeForReservation(reservation)):0
    }

    double getKMChrageForReservation(Reservation reservation){
        getDistanceTravelledInReservation(reservation) * PERKMCHARGE
    }

    double getMinuteChrageForReservation(Reservation reservation){
        getReservationDurationInMinutes(reservation) * PERMINCHARGE
    }

    double getPinkChargeForReservation(Reservation reservation){
        (reservation.isPink ? PINKCHARGE : 0)
    }

    long getReservationDurationInMinutes(Reservation reservation) {
        (reservation.end.time - reservation.start.time) / (1000 * 60)
    }


}
