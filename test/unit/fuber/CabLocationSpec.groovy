package fuber

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(CabLocation)
class CabLocationSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "Canary"() {
        when:''
        then:
        new CabLocation(cab: new Cab(),coordinate: [0,0])
    }

    void "Any CabLocation should have entry for cab"(){
        CabLocation cabLocation
        when: cabLocation = new CabLocation(coordinate:[0,0])
        then:
        !(cabLocation.validate())

    }

    void "Any CabLocation should have only two coordinates"(){
        CabLocation cabLocation
        when: cabLocation = new CabLocation(coordinate:coordinatesList)
        then:
        !(cabLocation.validate())
        where:
        sno | coordinatesList
        1   | []
        2   | [2]
        3   | [2,3,4]
    }
}
