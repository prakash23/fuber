package fuber

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
class DistanceUtilsSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }
    void "Distance of a coordinate with itself is 0"(){
        Random random= new Random()
        List coordinate=[random.nextDouble(),random.nextDouble()]
        double distance
        when:
       distance= DistanceUtils.getDistanceBetweencoordinates(coordinate,coordinate)
        then:
        distance==0
    }

    void "Difference of x coordinate with origin is x coordinate"(){
        Random random= new Random()
        double diff
        List coordinate=[random.nextDouble(),random.nextDouble()]
        when:
        diff= DistanceUtils.getDifferenceInXcoordinate(coordinate,[0,0])
        then:
        diff==coordinate.first()
    }
    void "Difference of y coordinate with origin is y coordinate"(){
        Random random= new Random()
        double diff
        List coordinate=[random.nextDouble(),random.nextDouble()]
        when:
        diff= DistanceUtils.getDifferenceInYcoordinate(coordinate,[0,0])
        then:
        diff==coordinate.last()
    }
}
