package fuber

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ReservationService)
@Mock([Reservation,Cab,CabLocation])
class ReservationServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void 'No bookings are made if no cab is available'() {
        when:''
        then:
        !(service.getAvailableCar([0,0],true).cabId)
    }

    void 'No pink bookings are made if no cab is available'() {
        when:''
        then:
        !(service.getAvailableCar([0,0],true).cabId)
    }

    void 'No pink bookings are made if only non-pink cabs are available'() {
        when:(1..5).each{ def cab= new Cab(isPink: false).save(); new CabLocation(cab:cab,isActive: true,coordinate: [1,2]).save() }
        then:
        !(service.getAvailableCar([0,0],true).cabId) && Cab.count==5
    }

    void 'Non pink bookings are made if only pink cabs are available'() {
        when:(1..5).each{ def cab= new Cab(isPink: true).save(); new CabLocation(cab:cab,isActive: true,coordinate: [1,2]).save() }
        then:
        (service.getAvailableCar([0,0],true).cabId) && Cab.count==5
    }

    void 'Reservation is Pink only if the cab provided and requested is pink'(){
        when:
        Reservation reservation
        def cab= new Cab(isPink: true).save();
        new CabLocation(cab:cab,isActive: true,coordinate: [1,2]).save()
        def map=service.getAvailableCar([0,0],true)
        reservation=Reservation.get(map.resId)
        then:
        reservation.isPink
    }


    void 'Reservation is not pink if the cab requested is not pink'(){
        when:
        Reservation reservation
        def cab= new Cab(isPink: true).save();
        new CabLocation(cab:cab,isActive: true,coordinate: [1,2]).save()
        def map=service.getAvailableCar([0,0],false)
        reservation=Reservation.get(map.resId)
        then:
        !(reservation.isPink)
    }

    void 'Reservation which starts and ends immediately has 0 duration'() {
        Reservation reservation
        when:
        reservation = new Reservation(start: new Date(),end: new Date())
        then:
        service.getReservationDurationInMinutes(reservation)==0
    }

    void 'Reservation Duration tests'(){
        Reservation reservation
        when:
        reservation = new Reservation(start: new Date(start),end: new Date(end))
        then:
        service.getReservationDurationInMinutes(reservation)==diff
        where:
        sno | start | end | diff
        1   | 2     | 2   | 0
        2   | 100   |100+60*1000| 1

    }

    void 'Reservation which starts and ends immediately has 0 MinuteCharge'() {
        Reservation reservation
        when:
        reservation = new Reservation(start: new Date(),end: new Date())
        then:
        service.getMinuteChrageForReservation(reservation)==0
    }

    void 'Reservation which starts and ends at same location has 0 distance'() {
        Reservation reservation
        when:
        Random random = new Random()
        double randomStart=random.nextDouble()
        double randomEnd=random.nextDouble()
        reservation = new Reservation(startCoordinate: [randomStart,randomEnd],endCoordinate: [randomStart,randomEnd])
        then:
        service.getDistanceTravelledInReservation(reservation)==0
    }

    void 'Reservation which starts and ends at same location has 0 KM Charge'() {
        Reservation reservation
        when:
        Random random = new Random()
        double randomStart=random.nextDouble()
        double randomEnd=random.nextDouble()
        reservation = new Reservation(startCoordinate: [randomStart,randomEnd],endCoordinate: [randomStart,randomEnd])
        then:
        service.getKMChrageForReservation(reservation)==0
    }

    void 'Pink Charge is zero if Reservation is Not Pink'() {
        Reservation reservation
        when:
        reservation = new Reservation(isPink: false)
        then:
        service.getPinkChargeForReservation(reservation)==0
    }

    void 'Pink Charge is applicable if Reservation is Pink'() {
        Reservation reservation
        when:
        reservation = new Reservation(isPink: true)
        then:
        service.getPinkChargeForReservation(reservation)==service.PINKCHARGE
    }


    void 'Non Pink Reservation which starts and ends at same location, and ends Immediately has 0 total Charge'() {
        Reservation reservation
        when:
        Random random = new Random()
        double randomStart=random.nextDouble()
        double randomEnd=random.nextDouble()
        reservation = new Reservation(startCoordinate: [randomStart,randomEnd],endCoordinate: [randomStart,randomEnd],start: new Date(),end:new Date())
        then:
        service.getAmountForReservation(reservation)==0
    }


    void 'Pink Reservation which starts and ends at same location, and ends Immediately has only Pink Charge'() {
        Reservation reservation
        when:
        Random random = new Random()
        double randomStart=random.nextDouble()
        double randomEnd=random.nextDouble()
        reservation = new Reservation(startCoordinate: [randomStart,randomEnd],endCoordinate: [randomStart,randomEnd],start: new Date(),end:new Date(),isPink: true,isRunning: false)
        then:
        service.getAmountForReservation(reservation)==service.PINKCHARGE
    }

    void 'Ending Coordinate for reservation becomes new active coordinate for cab'(){
        when:
        Reservation reservation
        def cab= new Cab(isPink: true).save();
        new CabLocation(cab:cab,isActive: true,coordinate: [1,2]).save()
        def map=service.getAvailableCar([0,0],false)
        reservation=Reservation.get(map.resId)
        service.endReservation(reservation,[3,4])
        CabLocation cabLocation= CabLocation.findByCabAndIsActive(cab,true)
        then:
        cabLocation.coordinate==[3,4]
    }

    void 'Charges for an Ongoing Reservation will be 0'(){
        when:
        Reservation reservation
        def cab= new Cab(isPink: true).save();
        new CabLocation(cab:cab,isActive: true,coordinate: [1,2]).save()
        def map=service.getAvailableCar([0,0],false)
        reservation=Reservation.get(map.resId)
        then:
        service.getAmountForReservation(reservation)==0
    }



}
