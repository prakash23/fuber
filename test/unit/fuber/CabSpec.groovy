package fuber

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(Cab)
class CabSpec extends Specification {

    List<Cab> allCabs
    List<Cab> boringCabs
    List<Cab> pinkCabs
    def setup() {
        Random random= new Random()
     allCabs=(1..20).collect{new Cab(isPink: (random.nextInt() %2 ))}
        boringCabs=allCabs.findAll{!(it.isPink)}
        pinkCabs=allCabs.findAll{(it.isPink)}
    }

    def cleanup() {
    }

    void "canary"() {
        when: ''
        then: new Cab()

    }

    void "The collection of cab contains pink and non pink cabs only"(){
        when:
            ''
        then:
            allCabs.size()==boringCabs.size()+pinkCabs.size()
    }

    void "A cab can either be pink or non pink "(){
        List<Cab> allCabs
        when:''
        then:
        boringCabs.intersect(pinkCabs)==[]
    }
}
